
FROM jupyter/scipy-notebook
LABEL maintainer="Muhammad Aditya Hilmy <mhilmy@hey.com>"

USER $NB_UID

RUN conda install python=3.8 \
    && conda install -y -c conda-forge python-gfal2 \
    && conda clean --all -f -y

USER root

RUN apt update -y \
    && apt install -y voms-clients-java \
    && apt clean -y \
    && rm /opt/conda/bin/voms-proxy-init \
    && ln -s /usr/bin/voms-proxy-init /opt/conda/bin/voms-proxy-init

RUN mkdir -p /opt/rucio-jl-docker && fix-permissions /opt/rucio-jl-docker
WORKDIR /opt/rucio-jl-docker
USER $NB_UID

# Increment this to force build
LABEL exp_version="9"

RUN pip install swanoauthrenew==1.0.1 PyJWT \
    && jupyter serverextension enable --py swanoauthrenew --sys-prefix 

RUN git clone https://github.com/rucio/jupyterlab-extension.git . \
    && git checkout feature/rucio-upload \
    && pip install -e . \
    && jupyter serverextension enable --py rucio_jupyterlab --sys-prefix  \
    && jupyter labextension link . --dev-build=False \
    && jupyter lab clean -y \
    && npm cache clean --force \
    && rm -rf "/home/${NB_USER}/.cache/yarn" \
    && rm -rf "/home/${NB_USER}/.node-gyp"


USER root

# Install ROOT
ENV LANG=C.UTF-8

ARG ROOT_BIN=root_v6.24.00.Linux-ubuntu20-x86_64-gcc9.3.tar.gz

WORKDIR /opt

COPY packages packages

RUN apt-get update -qq \
    && ln -sf /usr/share/zoneinfo/UTC /etc/localtime \
    && apt-get -y install $(cat packages) wget\
    && rm -rf /var/lib/apt/lists/*\
    && wget https://root.cern/download/${ROOT_BIN} \
    && tar -xzvf ${ROOT_BIN} \
    && rm -f ${ROOT_BIN} \
    && echo /opt/root/lib >> /etc/ld.so.conf \
    && ldconfig

ENV ROOTSYS /opt/root
ENV PATH $ROOTSYS/bin:$PATH
ENV PYTHONPATH $ROOTSYS/lib:$PYTHONPATH
ENV CLING_STANDARD_PCH none

# ESCAPE grid-security and VOMS setup
RUN apt update && apt -y install software-properties-common \
    && wget -q -O - https://dist.eugridpma.info/distribution/igtf/current/GPG-KEY-EUGridPMA-RPM-3 | apt-key add -

RUN apt update \
    && add-apt-repository 'deb http://repository.egi.eu/sw/production/cas/1/current egi-igtf core' \
    && apt -y install ca-certificates ca-policy-egi-core

RUN mkdir -p /etc/vomses \
    && wget https://indigo-iam.github.io/escape-docs/voms-config/voms-escape.cloud.cnaf.infn.it.vomses -O /etc/vomses/voms-escape.cloud.cnaf.infn.it.vomses

RUN mkdir -p /etc/grid-security/vomsdir/escape \
    && wget https://indigo-iam.github.io/escape-docs/voms-config/voms-escape.cloud.cnaf.infn.it.lsc -O /etc/grid-security/vomsdir/escape/voms-escape.cloud.cnaf.infn.it.lsc

# Setup merged CERN CA file
RUN mkdir /certs \
    && touch /certs/rucio_ca.pem \
    && cat /etc/grid-security/certificates/CERN-Root-2.pem >> /certs/rucio_ca.pem \
    && cat /etc/grid-security/certificates/CERN-GridCA.pem >> /certs/rucio_ca.pem

# Setup extension Rucio instance config
ADD bin/configure.py /opt/rucio-jl-docker/configure.py
ADD bin/configure.sh /usr/local/bin/configure.sh
RUN fix-permissions /opt/rucio-jl-docker \
    && fix-permissions /usr/local/bin/configure.sh \
    && sed -i -e 's/\r$/\n/' /usr/local/bin/configure.sh

# Default environment variables for ESCAPE Data Lake
ENV RUCIO_MODE=download
ENV RUCIO_WILDCARD_ENABLED=1
ENV RUCIO_BASE_URL=https://escape-rucio.cern.ch
ENV RUCIO_AUTH_URL=https://escape-rucio-auth.cern.ch
ENV RUCIO_WEBUI_URL=https://escape-rucio-webui.cern.ch
ENV RUCIO_DISPLAY_NAME="ESCAPE Data Lake"
ENV RUCIO_NAME=escape
ENV RUCIO_SITE_NAME=cern
ENV RUCIO_VOMS_ENABLED=1
ENV RUCIO_VOMS_VOMSES_PATH=/etc/vomses
ENV RUCIO_VOMS_CERTDIR_PATH=/etc/grid-security/certificates
ENV RUCIO_VOMS_VOMSDIR_PATH=/etc/grid-security/vomsdir
ENV RUCIO_CA_CERT=/certs/rucio_ca.pem
ENV RUCIO_DEFAULT_INSTANCE="escape"

RUN mkdir -p /opt/rucio \
    && chown -R $NB_UID /opt/rucio \
    && /usr/local/bin/configure.sh \
    && chown -R $NB_UID /etc/jupyter/jupyter_notebook_config.json \
    && chown -R $NB_UID /etc/jupyter/jupyter_notebook_config.py

# Auto load kernel extension when loading IPython
ENV IPYTHONDIR=/etc/ipython
ADD ipython_kernel_config.json /etc/ipython/profile_default/ipython_kernel_config.json
RUN chown -R $NB_UID /etc/ipython
ENV JUPYTER_ENABLE_LAB=yes

WORKDIR $HOME
USER $NB_UID

CMD ["configure.sh", "start-notebook.sh"]
