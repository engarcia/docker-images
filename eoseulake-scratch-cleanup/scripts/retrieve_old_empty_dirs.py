#!/usr/bin/python3

import os


def parse_key_value(line):
    data = {}
    for kvpair in line.split(' '):
        entry = kvpair.split('=')
        if len(entry) == 2:
            key, value = entry
            data[key] = value

    return data


def retrieve_empty_dirs(directory):
    paths = []
    stream = os.popen(f"eos find -d -p x --childcount {directory}")
    for entry in stream.readlines():
        data = parse_key_value(entry.strip())
        path = data['path']
        ndir = data['ndir']
        nfiles = data['nfiles']

        if path != directory and nfiles == '0' and ndir == '0':
            paths.append(path)

    return paths


def retrieve_old_dirs(directory, age_days=2):
    paths = []
    stream = os.popen(f"eos newfind -d -ctime +{age_days} {directory}")
    for entry in stream.readlines():
        path = entry.strip()
        if path != directory:
            paths.append(path)

    return paths


if __name__ == '__main__':
    os.system("eos whoami")  # somehow necessary to init proxy cert
    directory = '/' + os.environ['DIRECTORY_TO_CLEANUP'].strip("/") + '/'
    age_days = int(os.getenv('MAX_AGE_DAYS', "2"))

    empty_dirs = retrieve_empty_dirs(directory)
    old_dirs = retrieve_old_dirs(directory, age_days)
    for empty_dir in empty_dirs:
        if empty_dir in old_dirs:
            print(empty_dir)
