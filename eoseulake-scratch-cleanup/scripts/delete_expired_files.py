#!/usr/bin/python3

import os


def parse_key_value(line):
    data = {}
    for kvpair in line.split(' '):
        entry = kvpair.split('=')
        if len(entry) == 2:
            key, value = entry
            data[key] = value

    return data


def retrieve_expired_files(directory, age_days=2):
    paths = []
    stream = os.popen(f"eos find -f -ctime +{age_days} {directory}")
    for entry in stream.readlines():
        data = parse_key_value(entry)
        paths.append(data['path'])

    return paths


def delete_file(path):
    os.system(f"eos rm {path}")


if __name__ == '__main__':
    os.system("eos whoami")  # somehow necessary to init proxy cert

    directory = '/' + os.environ['DIRECTORY_TO_CLEANUP'].strip("/") + '/'
    age_days = int(os.getenv('MAX_AGE_DAYS', "2"))
    paths = retrieve_expired_files(directory, age_days)
    for path in paths:
        print("Deleting", path)
        delete_file(path)
